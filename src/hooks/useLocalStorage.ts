import { useEffect, useState } from "react";

function useLocalStorage() {
  let data = localStorage.getItem("woodstock");
  const [state, setState] = useState(data);

  const save = () => {
    state && localStorage.setItem("woodstock", state);
  };

  useEffect(() => {
    state && localStorage.setItem("woodstock", state);

    return () => {
      state && localStorage.setItem("woodstock", state);
    };
  }, [state, setState]);

  return [state, setState, save];
}

export default useLocalStorage;
