export let trunkSymbol = Symbol("path-store-trunk")

let pathStore = () => {
  let rootStore = new Map()

  let set = (path: any, value: any, store: any) => {
    store = store || rootStore

    switch (path.length) {
      case 0:
        store.set(trunkSymbol, value)
        break
      default:
        const [next, ...rest] = path
        let nextStore = store.get(next)
        if (!nextStore) {
          nextStore = new Map()
          store.set(next, nextStore)
        }
        set(rest, value, nextStore)
        break
    }
  }

  let get = (path: any, store: any): any => {
    store = store || rootStore

    switch (path.length) {
      case 0:
        return store.get(trunkSymbol)
        break
      default:
        const [next, ...rest] = path
        let nextStore = store.get(next)
        if (nextStore) {
          return get(rest, nextStore)
        } else {
          return undefined
        }
        break
    }
  }

  let del = (path: any, store: any) => {
    store = store || rootStore

    switch (path.length) {
      case 0:
        store.delete(trunkSymbol)
        
        break
      default:
        const [next, ...rest] = path
        let nextStore = store.get(next)
        if (nextStore) {
          del(rest, nextStore)
          if (!nextStore.size) {
            store.delete(next)
           
          }
        }
        break
    }
  }

  return { set, get, delete: del }
}

export default pathStore
