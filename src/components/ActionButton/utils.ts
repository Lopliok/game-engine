import { PrepareClasses } from "./types";

export const prepareClasses: PrepareClasses = (withAnimation: boolean) => {
  return withAnimation
    ? (clicked: boolean) => ({ className: clicked ? "animationClass" : "" })
    : () => {};
};
