type Props = {
  action: () => void;
  title: string;
  tooltip?: string;
  options: {
    animate?: boolean;
    duration?: number;
  };
};

type PrepareClasses = (
  c: boolean
) => ((clicked: boolean) => { className: string }) | (() => void);
type ActionButton = React.FunctionComponent<Props>;

export { Props as ActionButtonProps, PrepareClasses };
export default ActionButtonType;
