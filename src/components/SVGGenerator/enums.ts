export enum GroundType {
  path,
  tree,
  water,
  cave,
  nest,
  home
}
