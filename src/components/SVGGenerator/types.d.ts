import { GroundType } from "./enums"

//types.d.ts
export as namespace PlaygroungDataStructure

export type FieldType = {
  type: GroundType
  dangerLevel: 1 | 2 | 3
  visible: boolean
  visited: boolean
}

export type Field = {
  coords: number[]
  size: number[]
  xy: [number, number]
  char: string
  data: FieldType
  key: string
  svgProps: {
    x: number
    y: number
    fill: string
  }
}

export type PlaygroundMap = Map<string, Field>
