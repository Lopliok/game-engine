import React from "react"
import RenderTree, { OperationProps } from "./RenderTree";

export const RenderArray: React.FunctionComponent<OperationProps> = ({ itemKey, value}: OperationProps) => {
    const [open, setOpen] = React.useState(false);

    return (
      <div style={{ paddingLeft: 20}}>     
         <span>{`[`}</span>
         <div onClick={() => setOpen(!open)}>{!open ? "+" : "-"}</div>
  
          {open && Array.isArray(value) && value.map((item, index) => {      
            return <div key={`key-${index}`}> <RenderTree itemKey={index.toString()} value={item} /> </div>
          })} 
        <span>{`]`}</span> 
      </div>
    );
  }
  