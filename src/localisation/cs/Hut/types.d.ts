//types.d.ts
export as namespace HutLocalisation

export type HutLocal = {
  base: {
    items: {
      fire: {
        name: string
        tooltip: string
      }
    }
  }
  build: {
    title: string
    items: {
      trap: { name: string; tooltip: string }
      cart: { name: string; tooltip: string }
      hut: { name: string; tooltip: string }
    }
  }
}
